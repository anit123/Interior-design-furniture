import React from 'react';
import './learnMore.scss';
import { FaCheckCircle } from 'react-icons/fa';

const mainImage =
  'http://ebaytemplate.com/thesoft/website/idf/assets/img/overview.png';

const LearnMore = () => {
  return (
    <div className="learnMoreContainer">
      <div className="leftViewLearnMore">
        <img src={mainImage} alt="" />
      </div>
      <div className="RightViewLearnMore">
        <div className="smallHeadingLearnMore">
          <span>We are the best</span>
        </div>
        <div className="SecondmainHeadingLearnMore">
          <h2>Our Template build with</h2>
          <h2>awesome features!</h2>
        </div>
        <div className="learn-more-details">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod <br /> tempor incididunt ut labore et dolore
          </p>
        </div>
        <div className="learnMoreLists">
          <i className="listIconsLearnMore">
            {<FaCheckCircle />}{' '}
            <span>{'Decorate Your Dream With Our Products'}</span>
          </i>
        </div>
        <div className="learnMoreLists">
          <i className="listIconsLearnMore">
            {<FaCheckCircle />} <span>{'Expanded Warranty Services'}</span>
          </i>
        </div>
        <div className="learnMoreLists">
          <i className="listIconsLearnMore">
            {<FaCheckCircle />} <span>{'Free Return Service'}</span>
          </i>
        </div>
        <div className="learnMoreLists">
          <i className="listIconsLearnMore">
            {<FaCheckCircle />} <span>{'100% productive materials'}</span>
          </i>
        </div>
        <div className="learnMoreLists">
          <i className="listIconsLearnMore">
            {<FaCheckCircle />} <span>{'Free Installation'}</span>
          </i>
        </div>
        <div className="learnMoreLists">
          <i className="listIconsLearnMore">
            {<FaCheckCircle />} <span>{'24 X 7 Support'}</span>
          </i>
        </div>
        <div className="learnMoreButton">
          <button className="learnMoreButtonLink">Learn More</button>
        </div>
      </div>
    </div>
  );
};

export default LearnMore;
