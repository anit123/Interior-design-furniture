import React from 'react';
import Slideshow from '../slideShow/Slideshow';
import './ourProduct.scss';

const OurProducts = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-12">
          <div className="section-title">
            <h6 className="small-title">Interior Desing</h6>
            <h2>Our Product</h2>
          </div>
        </div>
      </div>
      <Slideshow />
    </div>
  );
};

export default OurProducts;
