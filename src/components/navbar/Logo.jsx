import React from 'react';
import './logo.scss';

function Logo() {
  return (
    <div>
      <div className="logo-wrapper">
        <span className="myLogo">IDF</span>
        <div className="heading">
          <h4>INTERIOR</h4>
          <h6>DESIGN AND FURNITURE</h6>
        </div>
      </div>
    </div>
  );
}

export default Logo;
