import React, { useState } from 'react';
import Slider from 'react-slick';
import { images } from './dataiImages';
import { FaArrowRight, FaArrowLeft } from 'react-icons/fa';
import '../slideShow/slideshow.css';

const Slideshow = () => {
  const NextArrow = ({ onClick }) => {
    return (
      <div className="arrow next" onClick={onClick}>
        <FaArrowRight />
      </div>
    );
  };

  const PrevArrow = ({ onClick }) => {
    return (
      <div className="arrow prev" onClick={onClick}>
        <FaArrowLeft />
      </div>
    );
  };

  const [imgIndex, setImgIndex] = useState(0);
  const settings = {
    infinite: true,
    lazyLoad: true,
    speed: 300,
    slidesToShow: 3,
    centerMode: true,
    centerPadding: 0,
    nextArrow: <PrevArrow />,
    prevArrow: <NextArrow />,
    beforeChange: (current, next) => setImgIndex(next),
  };

  return (
    <div className="slides-container">
      <Slider {...settings}>
        {images.map((img, idx) => {
          return (
            <div
              className={idx === imgIndex ? 'slide activeSlide' : 'slide'}
              key={idx}
            >
              <img src={img} alt={idx} />
            </div>
          );
        })}
      </Slider>
    </div>
  );
};

export default Slideshow;
