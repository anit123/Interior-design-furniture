import React from 'react';
import './featureTemplete.scss';
import { Images } from '../../images.js';
import { itemsData } from './futureItemsData';
const FeatureTemplete = () => {
  console.log(itemsData);
  return (
    <div className="container main">
      <div className="row d-flex justify-content-center">
        <div className="col-lg-6 col-md-12 col-sm-12">
          <div className="left-feature-container">
            <span className="small-heading-feature-container">
              About Our Features
            </span>
            <h2 className="second-main-heading">Decorate Your Dream</h2>
            <h2 className="second-main-heading">With Our Business!</h2>
            <div className="container" style={{ paddingLeft: '0' }}>
              <div className="row d-flex ">
                {itemsData.map((item) => {
                  const colorAnimation =
                    item.className == 'pulseP'
                      ? 'pulseP'
                      : null || item.className == 'pulseQ'
                      ? 'pulseQ'
                      : null || item.className == 'pulseR'
                      ? 'pulseR'
                      : null || item.className == 'pulseS'
                      ? 'pulseS'
                      : null || item.className == 'pulseT'
                      ? 'pulseT'
                      : null || item.className == 'pulseU'
                      ? 'pulseU'
                      : null;
                  return (
                    <div className="col-lg-6 col-md-6 col-sm-12 ">
                      <div className="future-item ">
                        <div className={colorAnimation}></div>
                        <div className="feature-info" key={item.name}>
                          <h3>{item.name}</h3>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="app-store-button">
              <p className="mb-20">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore
              </p>
              <a
                href="https://play.google.com/store/games?hl=en_US&gl=US"
                className="app-str-btn-1"
              >
                <img
                  src="http://ebaytemplate.com/thesoft/website/idf/assets/img/app-store-1.png"
                  alt=""
                />
              </a>
              <a href="#" className="app-str-btn-2">
                <img
                  src="http://ebaytemplate.com/thesoft/website/idf/assets/img/app-store-2.png"
                  alt=""
                />
              </a>
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-md-12">
          <div className="right-feature-container" style={{ float: 'right' }}>
            <img src={Images.ourFeature} alt="our feature" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeatureTemplete;
