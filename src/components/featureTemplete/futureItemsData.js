export const itemsData = [
  {
    name: 'Nice Design',
    className: 'pulseP',
  },
  {
    name: 'Durable',
    className: 'pulseQ',
  },
  {
    name: 'Cost Friendly',
    className: 'pulseR',
  },
  {
    name: '24X7 Support Available',
    className: 'pulseS',
  },
  {
    name: 'Buy Now Pay Later',
    className: 'pulseT',
  },
  {
    name: 'Easy Assemble',
    className: 'pulseU',
  },
];
