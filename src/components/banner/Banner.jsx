import React from 'react';
import './banner.scss';

const Banner = () => {
  return (
    <div className="bannerContainer">
      <span className="bannerHeading">
        <h1>Interior Design</h1>
        <h6>Furniture &amp; Design Hub!</h6>
        <p>IDF - Interior &amp; Furniture Design Studio, Enjoy Your Home</p>
      </span>
      <div className="colorOverlay"></div>
    </div>
    // <div>
    //   <div>
    //     <header className="banner_header">
    //       <img
    //       src={process.env.PUBLIC_URL + 'images/abc.jpeg'}
    //       className="bannerImage"
    //     />
    //       <span className="heading">
    //         <h1>Interior Design</h1>
    //         <h6>Awesome Furniture and Design Hub</h6>
    //       </span>
    //     </header>
    //   </div>
    // </div>
  );
};

export default Banner;
