import './App.css';
import NavComponent from './components/navbar/NavComponent';
import Banner from './components/banner/Banner';
import OurProducts from './components/ourProducts/OurProducts';
import FeatureTemplete from './components/featureTemplete/FeatureTemplete';
import LearnMore from './components/LearnMore/LearnMore';

function App() {
  return (
    <div className="App">
      <NavComponent />
      <Banner />
      <OurProducts />
      <FeatureTemplete />
      <LearnMore />
    </div>
  );
}

export default App;
